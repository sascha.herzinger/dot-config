abbr -a l3 ollama run llama3
abbr -a vim nvim
abbr -a vi nvim
abbr -a del rmtrash
abbr -a l ls -lhGp
abbr -a ll ls -lahGp

set -gx  LC_ALL en_US.UTF-8  

if status is-interactive
    # Commands to run in interactive sessions can go here
end
